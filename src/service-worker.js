/*this.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open('v1').then(function(cache) {
      console.log(" Service Worker Installed ");
      console.log(cache);
      return cache.addAll(["/"]); //Be carefull to this....
    }).catch(function() {
      console.log("Installation failed");
    }));
});*/

//Clone and Redirect URLs...
this.addEventListener('fetch', function(event) {
  //console.log("OURL " + event.request.url);
  var req = null;
  if(event.request.url.indexOf("https://www.localhost.com:4000") == 0 ){ // Also not redirect requests which are already redirected....
    req = event.request.clone();
  }else{
    var init = {};
    for(var key in event.request){
      if(key.toLowerCase() !== "url"){
        init[key] = event.request[key];
      }
    }
    req = new Request("https://www.middle.com:10000/r?type=src&ru=" + btoa(event.request.url), init);
  }
  console.log("OURL: " + event.request.url, "\nFURL: " + req.url)
  event.respondWith(
    caches.match(event.request).then(function(resp) {
      return fetch(req).then(function(response) {
        return response;
      });
    })
  );
});
