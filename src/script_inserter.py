def script_inserter(page, script):
    # Read File
    file_content = ""
    try:
        file_content = open(page).read()
    except:
        print ("Error Reading Page File ! Provide  Full Path")


	# Replace Service worker filename and scope

    if '<head>' in file_content:
        new_content = file_content.replace("<head>", "<head>\n\t\t<script src=" + script + "></script></script>")
    else:
        if "<html>" in file_content:
            new_content = file_content.replace("<html>", "<html>\n\t<head>\n\t\t<script src=" + script + "></script></script>\n\t</head>")
        else:
            new_content = "<html>\n\t<head>\n\t\t<script src=" + script + "></script>\n\t</head>\n</html>\n" + file_content

    # Save New File
    with open(page, 'w') as file :
        file.write(new_content)
