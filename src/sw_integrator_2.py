import argparse
import sys

from script_inserter import script_inserter

#Example :
#$ python3 sw_integrator.py ../ test www.inria.fr 88 www.mypro 90
def main(argv):
    """
    Args :
	-help : get get help
	home           Homepage html file path
	swpath         Path to install the service worker
    scope          Scope of the serviceè-worker
	durl           Domain url
	dport          Domain port
	purl           Proxy url
	pport          Proxy port
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("page",help="Html page file")
    parser.add_argument("script",help="Script code file")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",\
            action="store_true")
    args = parser.parse_args()
    verbose = args.verbose
    if verbose :
        print(str(args))

    if verbose :
        print("Copying Script Code " + args.script + " in " + args.page)
    script_inserter(args.page, args.script)
    if verbose:
        print("Code Copied.")

if __name__ == "__main__":
    main(sys.argv)
