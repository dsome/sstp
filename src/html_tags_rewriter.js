const purl = require('url'),
    jsdom = require('jsdom'),
    { JSDOM } = jsdom;





var htmlrewriter = {
    rwu : "https://sstp-middleparty.inria.fr",
    rhost : "https://sstp-rewriterproxy.inria.fr",
    dynamics : "/scripts/dynamic.js",

    init: function(rwu){
      rus = { // Content loading which is redirected to the middle party....
        "scripts":  rwu + "/r?type=src&ru=",
        "images":  rwu + "/r?type=src&ru=",
        "links": rwu + "/r?type=css&ru=",
        "anchors": rwu + "/r?type=src&ru=",
        "audios": rwu + "/r?type=src&ru=",
        "videos": rwu + "/r?type=src&ru=",
        "sources": rwu + "/r?type=src&ru=",
        "embeds": rwu + "/r?type=emb&ru=",
        "xhr": rwu + "/r?type=xhr&ru="
      },
    
      rustypes = { // Content loading which is redirected to the middle party....
        "src":  rwu + "/r?type=src&ru=",
        "css":  rwu + "/r?type=css&ru=",
        "emb": rwu + "/r?type=emb&ru=",
        "xhr": rwu + "/r?type=xhr&ru=",
        "open": rwu + "/r?type=open&ru=",
        "link": rwu + "/r?type=link&ru=",
      }
      return [rus, rustypes]
    },
  
    rProtocols : {
      "http:": "",
      "https:": "",
      "ws:": "",
      "wss:": ""
    },

    handleURL: function(url, type){
        return this.rustypes[type] + Buffer.from(url).toString('base64');
    },

    reWriteHTML: function(rbody, rhost, dynamics, rProtocols){
        var document = new JSDOM(rbody).window.document;
        var aLink = document.createElement("a");
        var rhost = purl.parse(rhost, true).host;
        var scripts = document.getElementsByTagName("script");
        for(var s=0; s<scripts.length; s++){
          var script = scripts[s];
          if(!script.type || script.type === "text/javascript" || (script.language && script.language.indexOf('javascript') >=0)){
            var srcc = script.getAttribute('src'); // Get the src value here
            if(srcc){
              aLink.href = srcc;
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
                script.setAttribute('src', this.handleURL(aLink.href, 'src'));
              }
             }
          }
        }

        var images = document.getElementsByTagName("img");
        for(var i=0; i<images.length; i++){
          var image = images[i];

            var srcc = image.getAttribute('src'); // Get the src value here
            if(srcc){
              aLink.href = srcc;
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost ) && (aLink.protocol in rProtocols)){
                image.setAttribute('src', this.handleURL(aLink.href, 'src'));
               }
             }
        }

        var links = document.getElementsByTagName("link");
        for(var l=0; l<links.length; l++){
          var link = links[l];

            var srcc = link.getAttribute('href'); // Get the src value here
            if(srcc){
              aLink.href = srcc;
              //console.log(aLink.href + " " +aLink.host + " " + aLink.origin);
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost)){
                link.setAttribute('href', this.handleURL(aLink.href, 'css'));
              }
             }
         }

        var links = document.getElementsByTagName("a");
        for(var l=0; l<links.length; l++){
          var link = links[l];

            var srcc = link.getAttribute('href'); // Get the src value here
            if(srcc){
              aLink.href = srcc;
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
                link.setAttribute('href', this.handleURL(aLink.href, 'link'));
              }
             }
        }

        var links = document.getElementsByTagName("area");
        for(var l=0; l<links.length; l++){
          var link = links[l];

            var srcc = link.getAttribute('href'); // Get the src value here
            if(srcc){
              aLink.href = srcc;
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
                link.setAttribute('href', this.handleURL(aLink.href, 'link'));
              }
             }
        }


          //Audios and videos sources
        var sources = document.getElementsByTagName("source");
         for(var s=0; s<sources.length; s++){
           var source = sources[s];

             var srcc = source.getAttribute('src'); // Get the src value here
             if(srcc){
               aLink.href = srcc;
               if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
                 source.setAttribute('src', this.handleURL(aLink.href, 'src'));
               }
              }
          }

            //Video poster images
        var videos = document.getElementsByTagName("video");
        for(var v=0; v<videos.length; v++){
          var video = videos[v];

            var poster = video.getAttribute('poster'); // Get the src value here
            if(poster){
              aLink.href = poster;
              if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
                console.log(aLink.href);
                video.setAttribute('poster', this.handleURL(aLink.href, 'src'));
              }
             }

            var srcc = video.getAttribute('src'); // Get the src value here
             if(srcc){
             aLink.href = srcc;
             if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
               video.setAttribute('src', this.handleURL(aLink.href, 'src'));
             }
            }
        }

        var audios = document.getElementsByTagName("audio");
        for(var v=0; v<audios.length; v++){
          var audio = audios[v];
            var srcc = audio.getAttribute('src'); // Get the src value here
             if(srcc){
             aLink.href = srcc;
             if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
               audio.setAttribute('src', this.handleURL(aLink.href, 'src'));
             }
            }
         }


         var iframes = document.getElementsByTagName("iframe");
         for(var i=0; i<iframes.length; i++){
           var iframe = iframes[i];
           var srcc = iframe.getAttribute('src');
           if(srcc){
             aLink.href = srcc;
             if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
               iframe.setAttribute('src', this.handleURL(aLink.href, 'emb'));
             }
           }
         }

         var iframes = document.getElementsByTagName("embed");
         for(var i=0; i<iframes.length; i++){
           var iframe = iframes[i];
           var srcc = iframe.getAttribute('src');
           if(srcc){
             aLink.href = srcc;
             if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
               iframe.setAttribute('src', this.handleURL(aLink.href, 'emb'));
             }
           }
         }

         var iframes = document.getElementsByTagName("object");
         for(var i=0; i<iframes.length; i++){
           var iframe = iframes[i];
           var srcc = iframe.getAttribute('data');
           if(srcc){
             aLink.href = srcc;
             if(!(aLink.origin == null || aLink.origin == "" || aLink.origin == undefined || aLink.host == rhost) && (aLink.protocol in rProtocols)){
               iframe.setAttribute('data', this.handleURL(aLink.href, 'emb'));
             }
           }
         }
         var head = document.getElementsByTagName("head")[0];
         var script = document.createElement("script");
         script.src = dynamics;
         if(head.firstChild){
           head.insertBefore(script, head.firstChild);
         }else{
           head.appendChild(script);
         }

         return document.documentElement.outerHTML;
      },

      rewriteFile: function(filePath, server, proxy, dynamics, generate){
        //Prefer the second method
        try{
          let content = fs.readFileSync(filePath)
          return this.rewriteContent(content.toString(), server, proxy, dynamics)
        }catch(e){
          return null
        }
      },

      rewriteContent: function(content, server, proxy, dynamics){
        this.rwu = proxy;
        this.rhost = server;
        this.dynamics = dynamics;
        res = this.init(this.rwu)
        this.rus = res[0];
        this.rustypes = res[1]
        return this.reWriteHTML(content, this.rhost, this.dynamics, this.rProtocols)
      }
}


module.exports = {
  htmlrewriter: htmlrewriter
};
