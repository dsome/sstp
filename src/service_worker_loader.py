import re
import os
import sys

def loader_inserter(filename, service_worker, scope):
	# Read File
	file_content = ""
	try:
		file_content = open(filename).read()
	except:
		print ("Error Reading File ! Provide  Full Path")

	# Replace Service worker filename and scope
	service_worker_loader = u"""
	\t\t\tif ('serviceWorker' in navigator) {
	\t\t\tnavigator.serviceWorker.register('/""" + service_worker.split("/")[-1] + """', { scope: '""" + scope + """' }).then(function(reg) {
	\t\t\t// registration worked
	\t\t\t   console.log('Registration succeeded. Scope is ' + reg.scope);
	\t\t\t }).catch(function(error) {
	\t\t\t   // registration failed
	\t\t\t   console.log('Registration failed with ' + error);
	\t\t\t });
	\t\t\t};"""

	if '<head>' in file_content:
		new_content = file_content.replace("<head>", "<head>\n\t\t<script>" + service_worker_loader + "\n\t\t</script>")
	else:
		if "<html>" in file_content:
			new_content = file_content.replace("<html>", "<html>\n\t<head>\n\t\t<script>\n" + service_worker_loader + "\n\t\t</script>\n\t</head>")
		else:
			new_content = "<html>\n\t<head>\n\t\t<script>" + service_worker_loader + "\n\t\t</script>\n\t</head>\n</html>\n" + file_content

    # Save New File
	with open(filename, 'w') as file :
		file.write(new_content)
