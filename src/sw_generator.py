def new_worker(domain_url,domain_port,proxy_url,proxy_port,output="test.js",template="service-worker.js", in_domain = "https://www.localhost.com:4000",in_proxy="https://www.middle.com:10000",drop_http_prefix=False):
    with open(template, "rt") as fin:
        with open(output, "wt") as fout:
            out_domain,out_proxy = \
                    prepare_out_domain_proxy(domain_url,domain_port,\
                    proxy_url,proxy_port,drop_http_prefix)
            for l in fin:
                fout.write(\
                        changed_line(l,in_domain,out_domain,in_proxy,out_proxy))

def changed_line(l,in_domain,out_domain,in_proxy,out_proxy):
    result = l
    if in_proxy is not None:
        result = result.replace(in_proxy,out_proxy)
    if in_domain is not None:
        result = result.replace(in_domain,out_domain)
    return result

def prepare_out_domain_proxy(domain_url,domain_port,proxy_url,proxy_port,drop_http_prefix):
    if domain_url is not None:
        if domain_port is None:
            domain_port = ""
        out_domain = domain_url+":"+str(domain_port)
    else:
        out_domain = None
    if proxy_url is not None:
        if proxy_port is None:
            proxy_port = ""
        out_proxy = proxy_url+":"+str(proxy_port)
    else:
        out_proxy = None
    if drop_http_prefix:
        out_proxy = out_proxy.replace("http://","")
        out_proxy = out_proxy.replace("https://","")
    return (out_domain,out_proxy)
