import argparse
import sys

from sw_generator import new_worker
from service_worker_loader import loader_inserter

#Example :
#$ python3 sw_integrator.py ../ test www.inria.fr 88 www.mypro 90
def main(argv):
    """
    Args :
	-help : get get help
	home           Homepage html file path
	swpath         Path to install the service worker
    scope          Scope of the serviceè-worker
	durl           Domain url
	dport          Domain port
	purl           Proxy url
	pport          Proxy port
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("home",help="Homepage html file path")
    parser.add_argument("swpath",\
            help="Path to install the service worker")
    parser.add_argument("scope",help="Scope of the service-worker")
    parser.add_argument("durl", help="Domain url")
    parser.add_argument("dport",help="Domain port")
    parser.add_argument("purl", help="Proxy url")
    parser.add_argument("pport",help="Proxy port")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",\
            action="store_true")
    args = parser.parse_args()
    verbose = args.verbose
    if verbose :
        print(str(args))
    sw_path = args.swpath + "/" + "service_worker.js"
    
    if verbose :
        print("Installing service worker in "+sw_path)
    new_worker(args.durl, args.dport, args.purl, args.pport, sw_path)
    if verbose:
        print("Done Installing.")
        print("Inserting service-worker loader in" + args.home)
    loader_inserter(args.home, sw_path, args.scope)
    if verbose:
        print("Done Inserting")



if __name__ == "__main__":
    main(sys.argv)
