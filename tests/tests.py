import unittest
import calendar
import time
from subprocess import check_output, call

import sys
sys.path.append('../src/')

from service_worker_loader import loader_inserter
from sw_generator import new_worker

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

    def test_service_worker_loader(self):
        # Create an html file
        pageWithHead = "<head></head>"
        pageWithoutHead = "<html></html>"
        pageWithoutHeadOrHtml = "<body></body>"
        service_worker = "../src/service_worker.js"
        with open("f1.test", "w") as f1:
            f1.write(pageWithHead)
        loader_inserter("f1.test", service_worker, "/")
        with open("f1.test", "r") as f1:
            f1content = f1.read()
            self.assertTrue("<head>" in f1content)
            self.assertTrue("service_worker.js" in f1content)

class TestJSRewrite(unittest.TestCase):

    def setUp(self):
        #Get timestamp
        file_seed = str(calendar.timegm(time.gmtime()))
        self.res_file_name = file_seed+"_tmp.js"

    def test_rewrite_without_dropping_url_prefix(self):
        new_worker("http://www.inria.fr",\
            23,"https://www.myproxy.fr",42,\
            output=self.res_file_name,template="template_1.js", \
            in_domain = "https://sstp-rewriterproxy.inria.fr",\
            in_proxy ="https://sstp-middleparty.inria.fr")
        ok = len(check_output(["diff","expected_1.js",\
                self.res_file_name])) == 0
        self.assertTrue(ok)

    def test_rewrite_with_dropping_url_prefix(self):
        new_worker(None,None,"https://www.myproxy.fr",42,\
            output=self.res_file_name,template="template_2.js", \
            in_domain = None,\
            in_proxy ="sstp-middleparty.inria.fr",
            drop_http_prefix=True)
        ok = len(check_output(["diff","expected_2.js",\
                self.res_file_name])) == 0
        self.assertTrue(ok)

    def test_rewrite_with_dropping_url_prefix_2(self):
        new_worker(None,"","www.myproxy.fr",42,\
            output=self.res_file_name,template="template_2.js", \
            in_domain = None,\
            in_proxy ="sstp-middleparty.inria.fr",
            drop_http_prefix=True)
        ok = len(check_output(["diff","expected_2.js",\
                self.res_file_name])) == 0
        self.assertTrue(ok)

    def tearDown(self):
        #Supprimer le fichier temporairement créé pour le test
        if self.res_file_name in str(check_output(["ls", "-l"])):
            call(["rm", self.res_file_name])
        pass

if __name__ == '__main__':
    unittest.main()
