
var assert = require('assert'),
	htr = require('../src/html_tags_rewriter'),
	fs  = require('fs')

describe('Array', function() {
        /*describe('#indexOf()', function() {
            it('should return -1 when the value is not present', function() {
                    assert.equal([1,2,3].indexOf(4), -1);
            });
        });*/
        describe("#HTML_TAG_REWRITER", function() {
        	it("Page Script URL transformed to based 64 prefixed with proxy URL", function() {
        		var fcontent = fs.readFileSync("page.htm"),
        			res = htr.htmlrewriter.rewriteContent(fcontent, "http://site.com", "http://proxy.com", "/scripts/dynamics.js")
        		assert.notEqual(res.indexOf('<script src="http://proxy.com/r?type=src&amp;ru=aHR0cHM6Ly90aGlyZC5jb20vc3cuanM="></script>'), -1)
        	});
        });
});