const http = require('http');
const purl = require('url');
const jsdom = require('jsdom');
const fs = require('fs');
var request = require('request');


const hostname = process.argv[2];
const port = process.argv[3];

const server = http.createServer((req, res) => {

  var irHeaders = req.headers;
  var irMethod = req.method;
  var irUrl = req.url;
  var body = [];


  req.on('error', function(err){
      console.log(err.stack);
  }).on('data', function(chunk){
    body.push(chunk);
  }).on('end', function(){
    body = Buffer.concat(body).toString();
    var surl = purl.parse(irUrl, true); //Read this in the URL query....
      //Return Homepage
      console.log(surl)
    if(surl.path == "/" || surl.path == "" || surl.path == "/index.htm"){
      //Return the home page
      res.setHeader("Content-Type", "text/html; charset=UTF-8");
      var str = getHomePage();
      res.write(str);
      res.end();
    }else if(surl.pathname.indexOf("/scripts/") == 0 || surl.path == "/service_worker.js"){
      res.writeHead(200, {
        "Content-Type": "application/javascript"
      });
      fs.createReadStream("." + surl.path).pipe(res);
    }else if(surl.pathname.indexOf("/data/") == 0){
      res.writeHead(200, {
        "Content-Type": "application/json"
      });
      fs.createReadStream("." + surl.path).pipe(res);
    }else{
      res.end();
    }
  });

});


server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});



function getHomePage(){
  try {
    return fs.readFileSync("index.htm")
  }catch(e){
    return ""
  }
}
